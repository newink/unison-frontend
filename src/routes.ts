import { Routes } from '@angular/router';
import { SignInComponent } from './app/components/auth/signin/signin.component';
import { SignUpComponent } from './app/components/auth/signup/signup.component';
import { PageNotFoundComponent } from './app/components/page-not-found/page-not-found.component';
import { ChatComponent } from './app/components/chat/chat.component';
import { AuthGuard } from './app/services/auth.guard';
import { InfoComponent } from './app/components/info/info.component';

export const routes: Routes = [
  {path: 'signin', component: SignInComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'chat', component: ChatComponent, canActivate: [AuthGuard]},
  {path: 'info', component: InfoComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: '/chat', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

