import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { AuthService } from './auth.service';

import * as SockJS from 'sockjs-client';
import { Stomp } from 'stompjs/lib/stomp.js';
import { Message } from '../components/chat/Message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public messages: Subject<Message> = new Subject<Message>();

  public subscribed = false;

  private readonly baseUrl = 'http://localhost:8080';

  private stompClient;

  constructor(private http: HttpClient,
              private authService: AuthService) {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(`${this.baseUrl}/ws`);
    this.stompClient = Stomp.over(ws);
    this.stompClient.connect({'Authorization': `Bearer ${this.authService.getToken()}`}, () => {
      this.stompClient.subscribe('/chat', (message) => {
        console.log(message);
        this.messages.next(JSON.parse(message.body));
      });
    });
  }

  sendMessage(message) {
    console.log(JSON.stringify(message));
    this.stompClient.send('/app/chat/send', {}, JSON.stringify(message));
  }

  public unsubscribe() {
    if (!this.subscribed) {
      return;
    }
    this.messages = null;
  }

  public getMessageHistory() {
    const headers = new HttpHeaders({'Authorization': `Bearer ${this.authService.getToken()}`});
    console.log(headers);
    return this.http.get(`${this.baseUrl}/chat/history`, {headers: headers});
  }

  private subscribe() {
    if (this.subscribed) {
      return;
    }
    this.subscribed = true;
  }
}
