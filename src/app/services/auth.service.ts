import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static LOCAL_STORAGE_KEY = 'chat-key';

  private baseUrl = 'http://localhost:8080';

  private token: string;

  private username: string;

  constructor(private http: HttpClient,
              private router: Router) {
    this.fetchToken();
    this.fetchInfo();
  }

  signUp(user: { username: string, password: string, birthDate: string }) {
    return this.http.post(`${this.baseUrl}/account/register`, user);
  }

  signIn(user: { username: string, password: string, rememberMe: boolean }) {
    return this.http.post(`${this.baseUrl}/account/login`, user);
  }

  setToken(token: string, store: boolean) {
    this.token = token;
    if (store) {
      localStorage.setItem(AuthService.LOCAL_STORAGE_KEY, token);
    }
  }

  public getToken() {
    return this.token;
  }

  public getUsername() {
    if (this.username === '') {
      this.fetchInfo();
    }
    return this.username;
  }

  public isAuthenticated() {
    const token = this.token;
    if (token == null || token.trim() === '') {
      this.fetchToken();
    }
    return token != null && token.trim() !== '';
  }

  public logout() {
    this.token = '';
    localStorage.removeItem(AuthService.LOCAL_STORAGE_KEY);
    this.router.navigate(['/signin']);
  }

  private fetchToken() {
    this.token = localStorage.getItem(AuthService.LOCAL_STORAGE_KEY);
  }

  private fetchInfo() {
    const headers = new HttpHeaders({'Authorization': `Bearer ${this.getToken()}`});
    this.http.get(`${this.baseUrl}/account/info`, {headers: headers}).subscribe((value: { username: string }) => {
      this.username = value.username;
      console.log(this.username);
    });
  }
}
