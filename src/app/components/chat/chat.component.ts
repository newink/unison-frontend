import { Component, OnInit } from '@angular/core';
import { Message } from './Message';
import { ChatService } from '../../services/chat.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['chat.component.css']
})
export class ChatComponent implements OnInit {

  typedMessage = '';

  messageHistory: Message[] = [];

  constructor(private chatService: ChatService,
              private authService: AuthService) {
  }

  onSend() {
    console.log(this.typedMessage);
    this.chatService.sendMessage(new Message(this.authService.getUsername(), this.typedMessage, new Date().toISOString(), 'sent'));
  }

  ngOnInit() {
    this.chatService.getMessageHistory().subscribe((value: { username: string, body: string, time: string }[]) => {
      this.messageHistory = value.map(message => {
        console.log(message.username === this.authService.getUsername());
        const type = message.username === this.authService.getUsername() ? 'sent' : 'received';
        return new Message(message.username, message.body, message.time, type);
      });
      console.log(this.messageHistory);
    });
    this.chatService.messages.subscribe((value) => {
      const type = value.username === this.authService.getUsername() ? 'sent' : 'received';
      this.messageHistory.push(new Message(value.username, value.body, value.time, type));
      console.log(this.messageHistory);
    });
  }

}
