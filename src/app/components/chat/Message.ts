export class Message {
  constructor(public username: string, public body: string, public time: string, public type: string) {
  }
}
