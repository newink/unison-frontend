import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {

  signinForm: FormGroup = new FormGroup({
    username: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required),
    remember: new FormControl()
  });

  alertShown = false;
  alertMessage = '';

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.signIn(this.signinForm.value).subscribe((value: { token: string }) => {
      this.authService.setToken(value.token, this.signinForm.get('remember').value);
      this.router.navigate(['chat']);
    }, (error) => {
      this.alertMessage = error.error.detail;
      if (this.alertMessage === '') {
        this.alertMessage = 'Unknown error';
      }
      this.alertShown = true;
    });
  }

}
