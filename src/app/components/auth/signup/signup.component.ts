import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {

  signupForm: FormGroup = new FormGroup({
    'username': new FormControl('', Validators.required),
    'password': new FormControl('', Validators.required),
    'birthDate': new FormControl('', Validators.required)
  });

  alertMessage = 'Unknown error';
  alertShown = false;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.signUp(this.signupForm.value).subscribe((value: { token: string }) => {
      this.authService.setToken(value.token, true);
      this.router.navigate(['chat']);
    }, error => {
      this.alertMessage = error.error.message;
      if (this.alertMessage === '') {
        this.alertMessage = 'Unknown error';
      }
      this.alertShown = true;
    });
  }
}
